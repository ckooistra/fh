CREATE TABLE Airlines (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  code VARCHAR(10) NOT NULL,
  name VARCHAR(100) NOT NULL,
  );


CREATE TABLE Airports (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  code VARCHAR(10) NOT NULL,
  city_code VARCHAR(100) NOT NULL,
  name VARCHAR(100) NOT NULL,
  city VARCHAR(100) NOT NULL,
  country_code VARCHAR(100) NOT NULL,
  region_code VARCHAR(100) NOT NULL,
  latitude FLOAT( 10, 6 ) NOT NULL,
  longitude FLOAT( 10, 6 ) NOT NULL,
  Timezone VARCHAR(100) NOT NULL,
  );


CREATE TABLE Flights (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  airline VARCHAR(10) NOT NULL,
  number INT(6) NOT NULL,
  departure_airport VARCHAR(10) NOT NULL,
  departure_time DATETIME NOT NULL,
  arrival_airport VARCHAR(100) NOT NULL,
  arrival_time DATETIME NOT NULL,
  price DECIMAL(10,2)
  );


INSERT INTO Airlines (code, name) VALUES ('AC', 'Air Canada')
INSERT INTO Airlines (code, name) VALUES ('WJ', 'West Jet')

INSERT INTO Airports (code, city_code, name, city, country_code, region_code, latitude, longitude, timezone) VALUES ('AC', 'A

INSERT INTO Airports (code, city_code, name, city, country_code, region_code, latitude, longitude, timezone) VALUES ('YEG', 'YEG', 'Edmonton International Airport', 'Edmonton', 'CA', 'AB', -113.580001831, 53.309700012200004, 'MDT');
INSERT INTO Airports (code, city_code, name, city, country_code, region_code, latitude, longitude, timezone) VALUES ('YHZ', 'YHZ', 'Halifax / Stanfield International Airport', 'Halifax', 'CA', 'NS', -63.5085983276, 44.8807983398, 'ADT');
INSERT INTO Airports (code, city_code, name, city, country_code, region_code, latitude, longitude, timezone) VALUES ('YOW', 'YOW', 'Ottawa Macdonald-Cartier International Airport', 'Ottawa', 'CA', 'ON', -75.66919708251953, 45.3224983215332, 'EDT');
INSERT INTO Airports (code, city_code, name, city, country_code, region_code, latitude, longitude, timezone) VALUES ('YUL', 'YMQ', 'Montreal / Pierre Elliott Trudeau International Airport', 'Montréal', 'CA', 'QC', -73.7407989502, 45.4706001282, 'EDT');
INSERT INTO Airports (code, city_code, name, city, country_code, region_code, latitude, longitude, timezone) VALUES ('YVR', 'YVR', 'Vancouver International Airport', 'Vancouver', 'CA', 'BC', -123.183998108, 49.193901062, 'PDT');
INSERT INTO Airports (code, city_code, name, city, country_code, region_code, latitude, longitude, timezone) VALUES ('YWG', 'YWG', 'Winnipeg / James Armstrong Richardson International Airport', 'Winnipeg', 'CA', 'MB', -97.2398986816, 49.909999847399995, 'CDT');
INSERT INTO Airports (code, city_code, name, city, country_code, region_code, latitude, longitude, timezone) VALUES ('YYC', 'YYC', 'Calgary International Airport', 'Calgary', 'CA', 'AB', -114.019996643, 51.113899231, 'MDT');
INSERT INTO Airports (code, city_code, name, city, country_code, region_code, latitude, longitude, timezone) VALUES ('YYJ', 'YYJ', 'Victoria International Airport', 'Victoria', 'CA', 'BC', -123.426002502, 48.646900177, 'PDT');
INSERT INTO Airports (code, city_code, name, city, country_code, region_code, latitude, longitude, timezone) VALUES ('YYT', 'YYT', 'St. John\'s International Airport', 'St. John\'s', 'CA', 'NL', -52.7518997192, 47.618598938, 'NDT');
INSERT INTO Airports (code, city_code, name, city, country_code, region_code, latitude, longitude, timezone) VALUES ('YYZ', 'YTO', 'Lester B. Pearson International Airport', 'Toronto', 'CA', 'ON', -79.63059997559999, 43.6772003174, 'EDT');

with open("canadian_airports.sql", "w") as f:
    for line in d_sub:
        print("INSERT INTO Airports (code, city_code, name, city, country_code, region_code, latitude, longitude, timezone) VALUES (row[9], 'xxxx', row[2], row[7], row[5], row[6].split("-")[-1], row[11][1:], row[12][1;-2], 'XXX')"

"INSERT INTO Airports (code, city_code, name, city, country_code, region_code, latitude, longitude, timezone) VALUES ('%s', 'xxxx', '%s', '%s', '%s', '%s', %s, %s, 'XXX')" % (row[9], row[2], row[7], row[5], row[6].split("-")[-1], row[11][1:], row[12][1;-2])
