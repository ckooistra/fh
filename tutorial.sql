-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 127.0.01    Database: tutorial
-- ------------------------------------------------------
-- Server version	5.5.5-10.5.10-MariaDB-1:10.5.10+maria~focal

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Airlines`
--

DROP TABLE IF EXISTS `Airlines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Airlines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Airlines`
--

LOCK TABLES `Airlines` WRITE;
/*!40000 ALTER TABLE `Airlines` DISABLE KEYS */;
INSERT INTO `Airlines` VALUES (1,'AC','Air Canada'),(2,'WJ','West Jet');
/*!40000 ALTER TABLE `Airlines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Airports`
--

DROP TABLE IF EXISTS `Airports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Airports` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `city_code` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `country_code` varchar(100) NOT NULL,
  `region_code` varchar(100) NOT NULL,
  `latitude` float(10,6) NOT NULL,
  `longitude` float(10,6) NOT NULL,
  `timezone` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Airports`
--

LOCK TABLES `Airports` WRITE;
/*!40000 ALTER TABLE `Airports` DISABLE KEYS */;
INSERT INTO `Airports` VALUES (1,'YEG','YEG','Edmonton International Airport','Edmonton','CA','AB',-113.580002,53.309700,'America/Edmonton'),(2,'YHZ','YHZ','Halifax / Stanfield International Airport','Halifax','CA','NS',-63.508598,44.880798,'America/Halifax'),(3,'YOW','YOW','Ottawa Macdonald-Cartier International Airport','Ottawa','CA','ON',-75.669197,45.322498,'America/Toronto'),(4,'YUL','YMQ','Montreal / Pierre Elliott Trudeau International Airport','Montréal','CA','QC',-73.740799,45.470600,'America/Toronto'),(5,'YVR','YVR','Vancouver International Airport','Vancouver','CA','BC',-123.183998,49.193901,'America/Vancouver'),(6,'YWG','YWG','Winnipeg / James Armstrong Richardson International Airport','Winnipeg','CA','MB',-97.239899,49.910000,'America/Winnipeg'),(7,'YYC','YYC','Calgary International Airport','Calgary','CA','AB',-114.019997,51.113899,'America/Edmonton'),(8,'YYJ','YYJ','Victoria International Airport','Victoria','CA','BC',-123.426003,48.646900,'America/Vancouver'),(9,'YYT','YYT','St. John\'s International Airport','St. John\'s','CA','NL',-52.751900,47.618599,'America/St_Johns'),(10,'YYZ','YTO','Lester B. Pearson International Airport','Toronto','CA','ON',-79.630600,43.677200,'America/Toronto');
/*!40000 ALTER TABLE `Airports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Flights`
--

DROP TABLE IF EXISTS `Flights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Flights` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `airline` varchar(10) NOT NULL,
  `number` int(6) NOT NULL,
  `departure_airport` varchar(10) NOT NULL,
  `departure_time` time NOT NULL,
  `arrival_airport` varchar(100) NOT NULL,
  `arrival_time` time NOT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Flights`
--

LOCK TABLES `Flights` WRITE;
/*!40000 ALTER TABLE `Flights` DISABLE KEYS */;
INSERT INTO `Flights` VALUES (1,'AC',2,'YYC','09:15:00','YUL','12:15:00',482.98),(2,'AC',2,'YEG','06:27:00','YUL','12:15:00',582.09),(3,'AC',2,'YUL','07:15:00','YYC','13:15:00',461.99),(4,'AC',2,'YEG','09:15:00','YYC','10:15:00',182.97),(5,'WJ',5,'YHZ','11:25:00','YOW','13:45:00',321.85),(6,'AC',5,'YHZ','16:25:00','YVR','18:32:00',624.96),(7,'WJ',5,'YHZ','13:46:00','YYZ','15:32:00',319.25),(8,'AC',5,'YOW','10:35:00','YWG','12:45:00',422.56),(9,'WJ',5,'YOW','06:22:00','YYJ','09:45:00',399.85),(10,'AC',5,'YOW','15:25:00','YYC','18:45:00',311.23),(11,'WJ',5,'YVR','11:25:00','YYC','13:45:00',121.85),(12,'AC',5,'YVR','07:25:00','YYT','14:03:00',446.91),(13,'WJ',5,'YVR','16:55:00','YUL','23:45:00',385.38),(14,'AC',5,'YUL','09:03:00','YWG','10:30:00',221.85),(15,'WJ',5,'YUL','11:25:00','YYZ','12:45:00',119.49),(16,'AC',5,'YUL','15:09:00','YHZ','16:38:00',199.99),(17,'WJ',5,'YWG','05:25:00','YYC','07:45:00',284.51),(18,'AC',5,'YWG','10:50:00','YOW','13:15:00',188.85),(19,'WJ',5,'YWG','14:25:00','YEG','16:45:00',210.45),(20,'AC',5,'YYC','12:25:00','YYT','19:45:00',428.75),(21,'WJ',5,'YYC','09:44:00','YOW','15:45:00',221.22),(22,'WJ',5,'YYC','06:44:00','YEG','08:05:00',121.45),(23,'WJ',5,'YYT','05:00:00','YYZ','07:45:00',111.11),(24,'WJ',5,'YYT','12:15:00','YUL','14:45:00',210.03),(25,'WJ',5,'YYZ','07:45:00','YYC','13:45:00',321.95),(26,'WJ',5,'YYZ','12:00:00','YVR','15:15:00',421.85),(27,'WJ',5,'YYZ','10:12:00','YEG','13:22:00',375.81);
/*!40000 ALTER TABLE `Flights` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-20 12:38:13
