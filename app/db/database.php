<?php
class Database{
  
    // specify your own database credentials
    private $host = "mysql";
    private $db_name = "tutorial";
    private $username = "tutorial";
    private $password = "secret";
    public $conn;
  
    // get the database connection
    public function getConnection(){
  
        $this->conn = null;
  
        try{
            $this->conn = new PDO("mysql:dbname=".$this->db_name.";host=".$this->host, $this->username, $this->password);
            $this->conn->exec("set names utf8");
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }
  
        return $this->conn;
    }
}
?>
