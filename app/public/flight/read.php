<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// database connection will be here

include_once '../../db/database.php';
include_once '../../objects/flight.php';

// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$flights = new Flight($db);
  
// read flights will be here
// query flights
$stmt = $flights->read();
$num = $stmt->rowCount();

if($num>0){
  
    // products array
    $flights_arr = [];
    $flights_arr["records"] = [];
  
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
  
        $flight_item=array(
            "id" => $id,
            "airline" => $airline,
            "number" => $number,
            "departure_airport" => $departure_airport,
            "departure_time" => $departure_time,
            "arrival_airport" => $arrival_airport,
            "arrival_time" => $arrival_time,
            "price" => $price
        );
  
        array_push($flights_arr["records"], $flight_item);
    }

	// set response code - 200 OK
    http_response_code(200);
  
    // show products data in json format
    echo json_encode($flights_arr);
} else {

	// set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no products found
    echo json_encode(
        array("message" => "No products found.")
    );
}
