<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// database connection will be here

include_once '../../db/database.php';
include_once '../../objects/trip.php';
include_once '../../objects/airports.php';

// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
$json = file_get_contents('php://input');
$data = json_decode($json);


function validateJsonTripData($d){
	if (
		!empty($d->departure_airport) &&
		!empty($d->arrival_airport) &&
		!empty($d->departure_date) && strtotime($d->departure_date) != false &&
		!empty($d->trip_type) && 
		(($d->trip_type == "round-trip" && !empty($d->return_date)) || $d->trip_type == "one-way") &&
		((!empty($d->return_date) && strtotime($d->return_date) != false) || empty($d->return_date))
	){
		return true;
	}
	else{
		return false;
	}
	
}

if (validateJsonTripData($data)){
	$trip = new Trip(
		$db,
		$data->departure_airport,
		$data->arrival_airport,
		$data->departure_date,
		!empty($data->return_date) ? $data->return_date : false,
		$data->trip_type,
		!empty($data->return_date) ? $data->return_date : false,
		!empty($data->fields) ? $data->fields : false,
		!empty($data->sort_order) ? $data->sort_order : "price",
		!empty($data->restrict_airline) ? $data->restrict_airline : false,
	);

	$flights = $trip->getFlights();
	$airports = new Airports($db);
	$trip->makeTrips($data->departure_airport, $data->arrival_airport);
	$trip->addWeights($airports->getAirports());
	$there_trips = $trip->getWeightedTrips();
	$return_trips = [];
	if ($data->trip_type == "round-trip"){
		$trip->setDateToUse("a");
		$trip->resetSimplePaths();
		$trip->makeTrips($data->arrival_airport, $data->departure_airport);
		$trip->addWeights($airports->getAirports());
		$return_trips = $trip->getWeightedTrips();
	}
	echo json_encode(array_merge($there_trips,$return_trips));
	//echo json_encode($trip->simple_paths);
	//echo json_encode($flights);

}
  
// read flights will be here
// query flights
//$stmt = $flights->read();
//$num = $stmt->rowCount();
//
//if($num>0){
//  
//    // products array
//    $flights_arr = [];
//    $flights_arr["records"] = [];
//  
//    // retrieve our table contents
//    // fetch() is faster than fetchAll()
//    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
//    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
//        // extract row
//        // this will make $row['name'] to
//        // just $name only
//        extract($row);
//  
//        $flight_item=array(
//            "id" => $id,
//            "airline" => $airline,
//            "number" => $number,
//            "departure_airport" => $departure_airport,
//            "departure_time" => $departure_time,
//            "arrival_airport" => $arrival_airport,
//            "arrival_time" => $arrival_time,
//            "price" => $price
//        );
//  
//        array_push($flights_arr["records"], $flight_item);
//    }
//
//	// set response code - 200 OK
//    http_response_code(200);
//  
//    // show products data in json format
//    echo json_encode($flights_arr);
//} else {
//
//	// set response code - 404 Not found
//    http_response_code(404);
//  
//    // tell the user no products found
//    echo json_encode(
//        array("message" => "No products found.")
//    );
//}
