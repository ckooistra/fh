<?php
class Airports{

	private $conn;
	private $table_name = "Airports";

	//object properties
	public $airport_arr;

	public function __construct($db){
		$this->conn = $db;
		$this->airport_arr = [];
	}

	public function getAirports(){
		$query = "SELECT *  FROM ". $this->table_name." ORDER BY Id DESC";
		// prepare query statement
    	$stmt = $this->conn->prepare($query);
  
    	// execute query
    	$stmt->execute();
		$num = $stmt->rowCount();

		if($num>0){
		  
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){

				extract($row);
		  
				$airport_item=array(
					"id" => $id,
					"code" => $code,
					"city_code" => $city_code,
					"name" => $name,
					"city" => $city,
					"country_code" => $country_code,
					"region_code" => $region_code,
					"latitude" => $latitude,
					"longitude" => $longitude,
					"timezone" => $timezone
				);
				$this->airport_arr[$code] = $airport_item;
			}
		}
    	return $this->airport_arr;
	}

}

?>
