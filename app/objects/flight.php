<?php
class Flight{

	private $conn;
	private $table_name = "Flights";

	//object properties
	public $airlines;
	public $restrict_airline;

	public function __construct($db,$restrict_airline = false){
		$this->conn = $db;
		$this->airlines = ["AC", "WJ"];
		$this->restrict_airline = $restrict_airline;
	}

	public function read(){
		$query = "SELECT *  FROM ". $this->table_name.$this->getWhereClause()." ORDER BY Id DESC";
		// prepare query statement
    	$stmt = $this->conn->prepare($query);
  
    	// execute query
    	$stmt->execute();
  
    	return $stmt;
	}
	public function getWhereClause(){
		
		if ($restrict_airline){
			return	" WHERE Airline = '$this->restrict_airline' ";
		}
		else{
			return "";
		}
	}
}


?>
