<?php
class Trip{

	private $conn;

	//object properties
	public $departure_airport;
	public $arrival_airport;
	public $departure_date;
	public $trip_type;
	public $return_date;
	public $fields;
	public $sort_order;
	public $weighted_data;
	public $restrict_airline;

	public function __construct($db,$da,$aa,$dd,$ad,$tt,$rd,$fi,$so,$ra){
		$this->conn = $db;
		$this->departure_airport = $da;
		$this->arrival_airport = $aa;
		$this->departure_date = $dd;
		$this->arrival_date = $ad;
		$this->trip_type = $tt;
		$this->return_date = $rd;
		$this->fields = $fi;
		$this->sort_order = $so;
		$this->restrict_airline = $ra;
		$this->f = null;
		$this->c = null;
		$this->visited = [];
		$this->curr_path = [];
		$this->simple_paths = [];
		$this->weighted_data = [];
		$this->date_to_use = "d";
	}

	public function getFlights(){

		$q = "SELECT * FROM Flights ";

		if ($this->restrict_airline){
			$q = $q.$this->getWhereClause();
			echo "got where clause -------$q";
		}


		// prepare query statement
    	$stmt = $this->conn->prepare($q);
  
    	// execute query
    	$stmt->execute();

		$num = $stmt->rowCount();

		$flights_arr = [];
		$flights_arr["flights"] = [];
		$flights_arr["cities"] = [];
		$f = &$flights_arr["flights"];
		$c = &$flights_arr["cities"];

		if($num>0){
		  
			// products array
		  
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){

				extract($row);
		  
				$flight_item=array(
					"id" => $id,
					"airline" => $airline,
					"number" => $number,
					"departure_airport" => $departure_airport,
					"departure_time" => $departure_time,
					"arrival_airport" => $arrival_airport,
					"arrival_time" => $arrival_time,
					"price" => $price
				);
				array_push($flights_arr["flights"], $flight_item);
			}


			foreach ($f as $val){
				$da = isset($c[$val["departure_airport"]]);
				$aa = isset($c[$val["arrival_airport"]]);
				
				if (!$da) $c[$val["departure_airport"]] = [ 'departures' => [], 'arrivals' => [] ];	
				if (!$aa) $c[$val["arrival_airport"]] = [ 'departures' => [], 'arrivals' => [] ];	

				if (isset($flights_arr["flights"][$val["id"]-1]["arrival_airport"])){
				
				
					$c[$val["departure_airport"]]['departures'][intval($val["id"])] = $flights_arr["flights"][$val["id"]-1]["arrival_airport"];
					$c[$val["arrival_airport"]]['arrivals'][intval($val["id"])] = $flights_arr["flights"][$val["id"]-1]["departure_airport"];
				}
			}

		} else {

			//handle this

		}
		//echo json_encode($flights_arr);
		$this->f = $flights_arr["flights"];
		$this->c = $flights_arr["cities"];
		return $flights_arr;
	}

	public function getWhereClause(){
		
		if ($this->restrict_airline){
			return	" WHERE Airline = '$this->restrict_airline' ";
		}
		else{
			return "";
		}
	}

	//Initial call to Dfs to determine all the flights
	public function makeTrips($d, $a){
		echo "call to makeTrips";
		$c = &$this->c;
		$f = &$this->f;

		$this->Dfs([$d, 0], $a);
		//echo json_encode($c);
		//$this->addWeights();
		return $this->simple_paths;
	}

	//Depth-first search to calculate all possible flights
	public function Dfs($u, $v){
		$c = &$this->c;
		$f = &$this->f;

		$w = &$this->visited;
		$cp = &$this->curr_path;
		$sp = &$this->simple_paths;
	
		if (isset($w[$u[0]]) && $w[$u[0]] == true){
			return;
		}
		$w[$u[0]] = true;
		array_push($cp, [$u[0],$u[1]]);

		if (strcmp($u[0], $v) == 0) {
			//echo "pushing to sp\n";
			array_push($sp, $cp);
			$w[$u[0]] = false;
			array_pop($cp);
			return;
		}
		if (isset($c[$u[0]]["departures"])){
			foreach ($c[$u[0]]["departures"] as $flightNo => $destination) {
				 $this->Dfs([$destination, $flightNo], $v);
			}
		}
		array_pop($cp);
		$w[$u[0]] = false;
	}

	//Date arithmatic used to determine the dates on which the flights land and layovers and what not.
	public function getMinutes($t1, $tz1, $t2, $tz2){
		$t1 = strtotime($t1);
		$t2 = strtotime($t2);
		if ($t2 < $t1){
			$t2 = strtotime("+1 day", $t2);
		}

		$dtt1 = new DateTime(date("Y-m-d H:i", $t1), new DateTimeZone($tz1));
		$dtt2 = new DateTime(date("Y-m-d H:i", $t2), new DateTimeZone($tz2));
		$interval = $dtt2->diff($dtt1);
		$diff_in_min = $interval->format('%H') * 60 + $interval->format('%I');

		return $diff_in_min;
	}

	public function getDaysSinceJan1($d_string){
		$first = new DateTime(date("Y")."-01-01");
		$d = new DateTime($d_string);
		return $d->diff($first)->format("%a");
	}

	public function resetSimplePaths(){
		$this->simple_paths = [];
	}
	
	public function getDateToUse(){
		return $this->date_to_use == "d"? $this->departure_date : $this->arrival_date;
	}

	public function getWeightedTrips(){
		return $this->weighted_data;
	}

	public function setDateToUse($date_to_use){
		$this->date_to_use = $date_to_use;
	}
	//Add cost and total minutes to trip for sorting
	public function addWeights($airports){

		$retVal = [];
		foreach($this->simple_paths as $index => $tr){

			$weighted_trips = ["flights" => [], 'price' => 0.0, 'minutes' => 0.0, "num_stops" => 0];

			foreach(array_slice($tr,1) as $i => $fl){

				$weighted_trips["num_stops"] += 1;
				$flight = &$this->f[$fl[1]-1];

				$weighted_trips["price"] += $flight["price"];

				$tz_departure = $airports[$flight["departure_airport"]]["timezone"];
				$tz_arrival = $airports[$flight["arrival_airport"]]["timezone"];

				$dt = $flight["departure_time"];
				$at = $flight["arrival_time"];

				$flight_mins = $this->getMinutes($dt, $tz_departure, $at, $tz_arrival);
				$layover_mins = 0;

				//echo "----$flight_mins-----";
				if (isset($tr[$i+2])){
					$nextFlight = &$this->f[$tr[$i+2][1]-1];
					$nextFlightDeparture = $nextFlight["departure_time"];
					$layover_mins += $this->getMinutes($at, $tz_arrival, $nextFlightDeparture, $tz_arrival);
				}

				$prev_flight = end($weighted_trips["flights"]); reset($weighted_trips["flights"]);
				$prev_layover = 0;
				if ($prev_flight){
					$prev_layover += $prev_flight["layover_till_next_fight_mins"];
				}

				$flight["layover_till_next_fight_mins"] = $layover_mins;

				$weighted_trips["minutes"] += $flight_mins;
				$weighted_trips["minutes"] += $layover_mins;

				$formattedFlight = $flight;
				//echo json_encode($formattedFlight);

				$formattedFlight["number"] = $formattedFlight["id"] * $this->getDaysSinceJan1($this->departure_date);
				$dep_date_time = $prev_flight ? new DateTime($prev_flight["arrival_datetime"], new DateTimeZone($tz_departure)) : new DateTime($this->getDateToUse()." ".$dt, new DateTimeZone($tz_departure));
				//$dep_date_time = new DateTime($this->departure_date." ".$dt, new DateTimeZone($tz_departure));
				$dep_date_time->add(new DateInterval("PT".$prev_layover."M"));

				$arri_date_time = clone $dep_date_time;
				$arri_date_time->add(new DateInterval("PT".$flight_mins."M"));
				$arri_date_time->setTimezone(new DateTimeZone($tz_arrival));

				unset($formattedFlight["departure_time"]);
				unset($formattedFlight["arrival_time"]);
				unset($formattedFlight["id"]);

				$formattedFlight["departure_datetime"] = $dep_date_time->format("Y-m-d H:i:s");
				$formattedFlight["arrival_datetime"] = $arri_date_time->format("Y-m-d H:i:s");

				array_push($weighted_trips["flights"], $formattedFlight);
			}

			array_push($retVal, $weighted_trips);
		}
		usort($retVal, [$this, "customSort"]);

		array_push($this->weighted_data, $retVal);
		//echo json_encode($retVal);
	}

	public function customSort($a, $b){
		if ($a[$this->sort_order] == $b[$this->sort_order]){
			return 0;
		}
		return $a[$this->sort_order] < $b[$this->sort_order] ? -1 : 1;
	}

}


?>
